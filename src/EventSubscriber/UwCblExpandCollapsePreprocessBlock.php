<?php

namespace Drupal\uw_cbl_expand_collapse\EventSubscriber;

use Drupal\core_event_dispatcher\Event\Form\FormAlterEvent;
use Drupal\core_event_dispatcher\Event\Form\FormBaseAlterEvent;
use Drupal\core_event_dispatcher\Event\Form\FormIdAlterEvent;
use Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent;
use Drupal\uw_cfg_common\CustomBlocks\UwCblBase;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class UwCblCopyTextPreprocessBlock.
 */
class UwCblExpandCollapsePreprocessBlock extends UwCblBase implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      BlockPreprocessEvent::name() => 'preprocessBlock',
    ];
  }

  /**
   * Preprocess blocks with expand/collapse and set variables.
   *
   * @param \Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent $event
   *   The event.
   */
  public function preprocessBlock(BlockPreprocessEvent $event): void {

    // Check if we are on the right block to preprocess.
    if ($this->checkPreprocessBlock($event, 'uw_cbl_expand_collapse')) {

      // Get the variables from the event.
      $variables = $event->getVariables();

      // Load in the block
      $block = $variables->getByReference('content')['#block_content'];

      // Get the expand/collapse items from the block object.
      $items = $block->field_uw_ec_items->getValue();

      // Step through each of the items and get out the information.
      foreach ($items as $item) {

        // Check if the text details has a target_id.
        if (isset($item['target_id']) && $item['target_id']) {

          // If there is an existing item it will have a target_id,
          // and we need to load in the paragraph.
          $para = \Drupal\paragraphs\Entity\Paragraph::load($item['target_id']);
        }
        else {

          // If it is a new item we need to load in the entity.
          $para = $item['entity'];
        }

        // Set the variables from the paragraph.
        $ecs[] = [
          'title' => $para->field_uw_ec_title->value,
          'text' => $para->field_uw_ec_text->value,
        ];
      }

      // If there are expand/collapse at to variables so template can use it.
      if (isset($ecs)) {

        // Set the block id which is the uuid of the block,
        // this is the unique identifier we need to identify
        // each different expand/collapse.
        $variables->set('block_id', $block->uuid());

        // All the expand/collapse items.
        $variables->set('ecs', $ecs);
      }
    }
  }
}
